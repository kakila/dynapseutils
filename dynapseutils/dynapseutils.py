#!/usr/bin/env python3
# dynapseutils.py

# MIT License
#
# Copyright (c) 2018 Juan Pablo Carbajal
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

try:
    import CtxDynapse as ctx
except:
    pass

import numpy as np

from synapse import (
    SynapseType,
    Synpase
)
from neuron import (
    Neuron,
    SpikeGenerator
)

class DynapSE:
    ''' Adapts and abstracts CtxDynapse.DynapseModel '''

    #: Reference to CtxDynapse.model
    _model = None
    #: Indexes into cls.neurons of neurons in a given chip
    _neurons_in_chip = None
    #: Indexes into cls.neurons of neurons in a given chip and core
    _neurons_in_core = None

    numChips = 4
    numCoresPerChip = 4
    numNeuronsPerCore = 256
    numNeuronsPerChip = 1024
    numNeurons = 4096

    #: 3D array indexing chip, core, and neuron
    #: re-organizes the result of CtxDynapse.model.get_neurons()
    neurons = np.zeros((numChips, numCoresPerChip, numNeuronsPerCore), dtype=np.dtype(DynapSE))

    def __init__(self, model):
        if self._model is None:
            self._model = model

            # Temporary hold all neurons in a list
            _neurons = [Neuron(n) for n in self._model.get_neurons()]

            # Get indexes per chip, and chip and core
            n_in_c = [None] * self.numChips
            n_in_cc = [[None] * self.numCoresPerChip for k in range(self.numChips)]
            for chip in range(self.numChips):
                n_in_c[chip] = tuple(_neurons.index(n) for n in _neurons if n.chipid == chip)
                for core in range(self.numCoresPerChip):
                    n_in_cc[chip][core] = tuple(i for i in n_in_c[chip] if _neurons[i].coreid == core)
                n_in_cc[chip] = tuple(n_in_cc[chip])

            self._neurons_in_chip = tuple(n_in_c)
            self._neurons_in_core = tuple(n_in_cc)

            # Organize according to chip and core
            for chip in range(self.numChips):
                for core in range(self.numCoresPerChip):
                    self.neurons[chip, core, :] = [
                        _neurons[n] for n in self._neurons_in_core[chip][core]]
            # Since this is the organization of the hardware it can't be modified
            self.neurons.flags.writeable = False

def demo():
    '''
    Output:

    Total number of neurons: 4096
    Number of neurons in chip 0: 1024
      in core 0: 256
      in core 1: 256
      in core 2: 256
      in core 3: 256
    Number of neurons in chip 1: 1024
      in core 0: 256
      in core 1: 256
      in core 2: 256
      in core 3: 256
    Number of neurons in chip 2: 1024
      in core 0: 256
      in core 1: 256
      in core 2: 256
      in core 3: 256
    Number of neurons in chip 3: 1024
      in core 0: 256
      in core 1: 256
      in core 2: 256
      in core 3: 256
    '''
    dynap = DynapSE(ctx.model)

    # Count all neurons
    print('Total number of neurons: %d' % np.prod(dynap.neurons.shape))
    for chip, nc in enumerate(dynap.neurons):
        print('Number of neurons in chip %d: %d' % (chip, nc.size))
        for core, ncc in enumerate(nc):
            print('  in core %d: %d' % (core, ncc.size))

    # Build spike generator
    # Maybe reuse from here https://github.com/sanfans/DYNAPSETools
    spiker = SpikeGenerator ()

    # Build Synapses
    # Return functions (partials) or callables
    fast_synapse = Synapse(stype=SynapseType.EXCITATORYFAST, ...) # parameters can be defined here or later
    slow_synapse = Synapse(stype=SynapseType.EXCITATORYSLOW) # parameters

    # Build simple network
    #       spiker
    #         |
    #         v
    #  1 <-- 'A' --> 'Output'
    edges = [
        slow_synapse(source=spiker, dest='A', ...), # here the missing parameters
        fast_synapse(source='A', dest=1),
        fast_synapse(source='A', dest='Output')
    ]
    # Equivalent
    #edges = [
    #    slow_synapse(source=spiker, dest='A'),
    #    fast_synapse(source='A', dest=[2, 3])
    #]

    # Build network
    # Should also be able to read adjacency matrix, graphviz, networkx, ...
    vstruct = Network(edges=edges);

    # Instantiate the abstract network into the device
    # Returns an instantiated network, i.e. hardware aware
    # the method accepts a list of neurons to be used, otherwise it chooses automatically
    vstruct = dynap.instantiate(vstruct)

    from scipy.sparse import coo_matrix
    A = coo_matrix(([1,1,1], ([0,1,1],[1,2,3])))
    vstruct_adj = Network(adjacency=A, edges=edges)
    # Instantiate the abstract network into the device using other hardware networks
    # Returns an instantiated network, i.e. hardware aware
    # Faisl if there is no room to produce and independent network
    vstruct_adj = dynap.instantiate(vstruct_adj)

if __name__ == '__main__':

    demo()
