#!/usr/bin/env python3
# neuron.py

# MIT License
#
# Copyright (c) 2018 Juan Pablo Carbajal
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from collections import namedtuple

#: Represets the coordinate of a neuroin in hardware addreses
#: 'neuron' is the neuron id (0-255), not confuse with Neuron.label
#: 'core' is the core number to which the neuron belongs (0-3).
#: 'chip' is the chip number the core belongs to (0-3).
_NeuronHardwareID = namedtuple('NeuronHWID', ['chip', 'core', 'neuron'])

class Neuron:
    ''' Adaptor for CtxDynapse.DynapseNeuron '''

    def __init__(self, neuron):
        self._neuron = neuron
        self.hrdwid = _NeuronHardwareID(self._neuron.get_chip_id(),
                                       self._neuron.get_core_id(),
                                       self._neuron.get_neuron_id()
                                      )

    @property
    def chipid(self):
        return self.hrdwid.chip

    @property
    def coreid(self):
        return self.hrdwid.core

    @property
    def neuronid(self):
        return self.hrdwid.neuron

    @property
    def label(self):
        return self._neuron.get_id()

class SpikeGenerator:
    '''Adapts SpikeGen'''
    pass
