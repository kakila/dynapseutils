#!/usr/bin/env python3
# synapse.py

# MIT License
#
# Copyright (c) 2018 Juan Pablo Carbajal
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from enum import Enum

class SynapseType(Enum):
    ''' Encapsulates types of synapses (neuron-to-neuron connections)

        DyanpSE has two types of synapses: inhibitory and exitatory, and they
        come in two flavors: fast and slow, making a total numer of 4 types
    '''
    INHIBITORYSLOW = 0
    INHIBITORYFAST = 1
    EXCITATORYFAST = 2
    EXCITATORYSLOW = 3


class Synapse:
  pass
