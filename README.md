## Development plans

### Accessing functions

* Views on Neuron/Chips/Groups/Cores)

### Input/Output

* Singal to spikes (using generators: encoders and decoders for completeness)

### Graphs

* Neuron parents and childs
* predicate: isconnected
* Root neurons
* Leaf neurons
* Adjacency to connectivity

### Neuron parameters

* Parameters metadata: control, description, tags
* Parameters subset creation
* Accessing only parameters subsets


### Plots/IO

* Topology (with only connected neurons)
* Topology <--> adjacency matrix
* Topology <--> networkx

### Examples

* v-structure
* immorality (collider)
* confounder
* Academic examples
