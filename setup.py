# -*- coding: utf-8 -*-
# vim: set tabstop=4
# setup.py
#!/usr/bin/env python3
""" setup script for dynaprseutils module """

# MIT License
#
# Copyright (c) 2018 Juan Pablo Carbajal
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>

from setuptools import (
    setup,
    find_packages
)

from dnyapseutils import (
    __version__,
    __author__,
    __email__
)

setup (
    name="dynapseutils",
    packages=find_packages(),
    version=__version__,
    install_requires=[
                    "ctxctl>=0.1.0",
                    "numpy>=1.12.1",
		     ],
    author=__author__,
    author_email=__email__,
    url="www.gitlab.com/kakila/dynapseutils",
    classifiers=["Programming Language :: Python :: 3", 
                 "Operating System :: OS Independent",
                 "License :: OSI Approved :: MIT License",
                 "Development status :: 3 - Alpha",
                 "Intended Audience :: Developers",
                 "Intended Audience :: Information Technology",
                 "Intended audience :: Science/Research",
                 "Environment :: Other Environment",
                 "Topic :: Scientific/Engineering",
                 "Topic :: Scientific/Engineering :: Artificial Intelligence",
                 "Topic :: Scientific/Engineering :: Information Analysis",
                 "Topic :: Utilities",
                 "Topic :: System :: Hardware",
                 ],
    description="TODO",
    long_description=" TODO ",
)
